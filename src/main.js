var size = undefined
var c = [
  // [posX, posY, size]
  [-0.02, -0.01, 0.21],
  [0.24, -0.123, 0.42],
  [-0.14, -0.01, 0.22],
  [-0.187, 0.04, 0.213],
  [-0.135, 0.19, 0.297],
  [-0.161, 0.04, 0.188],
  [-0.05, 0.075, 0.125],
  [0.2, 0.2, 0.5],
  [0.025, 0.075, 0.15],
  [0.05, -0.075, 0.1]
]
var t = [
  // [posX, posY, size, rotation]
  [0.23, 0.2, 0.28, 0.19],
  [-0.16, 0, 0.123, 0.249],
  [0.179, -0.0257, 0.19, 0.83],
  [-0.168, -0.045, 0.135, 0.56],
  [0.107, -0.145, 0.235, 0.158],
  [0.168, -0.045, 0.169, 0.46],
  [0.275, 0, 0.275, 0.45],
  [-0.275, 0.05, 0.25, 0.52],
  [0.025, -0.076, 0.075, 0.057],
  [-0.2, 0.15, 0.27, 0.57]
]
var step = 0
var animation = {
  bool: false,
  speed: 0.00025,
  state: 0
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  if (windowWidth >= windowHeight) {
    size = windowHeight * 0.95
  } else {
    size = windowWidth * 0.95
  }
}

function draw() {
  background(255)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  if (animation.bool === true) {
    push()

    translate(windowWidth * 0.5, windowHeight * 0.5)
    fill(0)
    noStroke()
    rect(0, 0, size * 0.35, size * 0.35)

    fill(255)
    noStroke()
    ellipse(
      size * (c[step][0] + (c[(step + 1) % 10][0] - c[step][0]) * animation.state),
      size * (c[step][1] + (c[(step + 1) % 10][1] - c[step][1]) * animation.state),
      size * (c[step][2] + (c[(step + 1) % 10][2] - c[step][2]) * animation.state)
    )
    push()
    translate(
      size * (t[step][0] + (t[(step + 1) % 10][0] - t[step][0]) * animation.state),
      size * (t[step][1] + (t[(step + 1) % 10][1] - t[step][1]) * animation.state)
    )
    polygon(
      size * (t[step][2] + (t[(step + 1) % 10][2] - t[step][2]) * animation.state),
      Math.PI * (t[step][3] + (t[(step + 1) % 10][3] - t[step][3]) * animation.state)
    )
    pop()

    noFill()
    stroke(0)
    strokeWeight(1)
    rect(0, 0, size * 0.35, size * 0.35)
    ellipse(
      size * (c[step][0] + (c[(step + 1) % 10][0] - c[step][0]) * animation.state),
      size * (c[step][1] + (c[(step + 1) % 10][1] - c[step][1]) * animation.state),
      size * (c[step][2] + (c[(step + 1) % 10][2] - c[step][2]) * animation.state)
    )
    push()
    translate(
      size * (t[step][0] + (t[(step + 1) % 10][0] - t[step][0]) * animation.state),
      size * (t[step][1] + (t[(step + 1) % 10][1] - t[step][1]) * animation.state)
    )
    polygon(
      size * (t[step][2] + (t[(step + 1) % 10][2] - t[step][2]) * animation.state),
      Math.PI * (t[step][3] + (t[(step + 1) % 10][3] - t[step][3]) * animation.state)
    )
    pop()

    pop()

    animation.state += deltaTime * animation.speed
    if (animation.state >= 0.999) {
      step = (step + 1) % 10
      animation.bool = false
      animation.state = 0
    }
  } else {
    push()

    translate(windowWidth * 0.5, windowHeight * 0.5)
    fill(0)
    noStroke()
    rect(0, 0, size * 0.35, size * 0.35)

    fill(255)
    noStroke()
    ellipse(size * c[step][0], size * c[step][1], size * c[step][2])
    push()
    translate(size * t[step][0], size * t[step][1])
    polygon(size * t[step][2], Math.PI * t[step][3])
    pop()

    noFill()
    stroke(0)
    strokeWeight(1)
    noStroke()
    rect(0, 0, size * 0.35, size * 0.35)
    ellipse(size * c[step][0], size * c[step][1], size * c[step][2])
    push()
    translate(size * t[step][0], size * t[step][1])
    polygon(size * t[step][2], Math.PI * t[step][3])
    pop()

    pop()
  }
}

function drawNumber(number, size) {
  if (number === 1) {
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    rect(0, 0, size, size)
    pop()
  }
}

function keyPressed() {
  if (keyCode === 32 || keyCode === 13) {
    animation.bool = true
  }
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
  if (windowWidth >= windowHeight) {
    size = windowHeight * 0.95
  } else {
    size = windowWidth * 0.95
  }
}

function polygon(radius, a) {
  let angle = TWO_PI / 3
  push()
  rotate(a)
  beginShape()
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = cos(a) * radius
    let sy = sin(a) * radius
    vertex(sx, sy)
  }
  endShape(CLOSE)
  pop()
}
